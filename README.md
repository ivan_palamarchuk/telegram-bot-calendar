# Telegram-bot-calendar is the InlineKeyboardMarkup calendar for [Telegram bot api](https://github.com/rubenlagus/TelegramBots)

![en](photo/En example.png) ![ru](photo/Ru example.png)

## Available languages
`
Russian "ru"
English "en"`

## Methods
1) `List<BotApiMethod<?>> getDefaultCalendar(String chatId)`

`List<BotApiMethod<?>> getDefaultCalendar(String chatId, String language)`

Returns instance of the calendar with the current date and selected language(or English by default).

2) `public List<BotApiMethod<?>> handleEmptyCalendarAnswer(Update update)`

`public List<BotApiMethod<?>> handleEmptyCalendarAnswer(Update update, String language)`

Method for handling calendar cells with no date inside
If selected button is `Change month button` delete the previous message with a calendar and create a new Calendar for the selected month.
Returns List with `DeleteMessage` and `SendMessage`.
If another button selected return empty `List<BotApiMethod<?>>`.

3)`LocalDate getCalendarDate(Update update)`
Method for handling Calendar cells with date.
Returns date of selected cell.

##### For questions and suggestions the.ivan.palamarchuk@gmail.com
