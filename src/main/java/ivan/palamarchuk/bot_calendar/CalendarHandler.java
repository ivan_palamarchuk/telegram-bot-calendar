package ivan.palamarchuk.bot_calendar;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class CalendarHandler {

    public List<BotApiMethod<?>> getDefaultCalendar(String chatId) {
        TGCalendar.setLanguage("en");
        InlineKeyboardMarkup cal = new TGCalendar(YearMonth.now().getYear(), YearMonth.now().getMonthValue()).getCalendar();
        SendMessage sendMessage = new SendMessage(chatId, "Choose the date");
        sendMessage.setReplyMarkup(cal);
        return List.of(sendMessage);
    }

    public List<BotApiMethod<?>> getDefaultCalendar(String chatId, String language) {
        TGCalendar calendar = new TGCalendar(YearMonth.now().getYear(), YearMonth.now().getMonthValue());
        TGCalendar.setLanguage(language);
        InlineKeyboardMarkup cal = calendar.getCalendar();
        String text;
        if (language.equals("ru")) {
            text = "Выберите дату";
        } else {
            text = "Choose the date";
        }
        SendMessage sendMessage = new SendMessage(chatId, text);
        sendMessage.setReplyMarkup(cal);
        return List.of(sendMessage);
    }

    public List<BotApiMethod<?>> handleEmptyCalendarAnswer(Update update) {
        String callback = update.getCallbackQuery().getData().split("\\.{3}")[1];
        TGCalendar.setLanguage("en");
        List<BotApiMethod<?>> messages = new ArrayList<>();
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        int msgId = update.getCallbackQuery().getMessage().getMessageId();
        if (Pattern.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", callback) && !update.getCallbackQuery().getData().isEmpty()) {
            handleChangeMonthButton(callback, messages, chatId, msgId, "en");
        }
        return messages;
    }

    public List<BotApiMethod<?>> handleEmptyCalendarAnswer(Update update, String language) {
        String callback = update.getCallbackQuery().getData().split("\\.{3}")[1];
        List<BotApiMethod<?>> messages = new ArrayList<>();
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        int msgId = update.getCallbackQuery().getMessage().getMessageId();
        if (Pattern.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", callback) && !update.getCallbackQuery().getData().isEmpty()) {
            handleChangeMonthButton(callback, messages, chatId, msgId, language);
        }
        return messages;
    }

    private void handleChangeMonthButton(String callback, List<BotApiMethod<?>> messages, String chatId, int msgId, String language) {
        DeleteMessage delMsg = new DeleteMessage();
        delMsg.setChatId(chatId);
        delMsg.setMessageId(msgId);
        messages.add(delMsg);

        LocalDate date = LocalDate.parse(callback);
        TGCalendar calendar = new TGCalendar(date.getYear(), date.getMonthValue());
        TGCalendar.setLanguage(language);
        InlineKeyboardMarkup cal = calendar.getCalendar();
        SendMessage sendMessage = new SendMessage(chatId, "Please, choose the date");
        sendMessage.setReplyMarkup(cal);
        messages.add(sendMessage);
    }

    public LocalDate getCalendarDate(Update update) {
        return LocalDate.parse(update.getCallbackQuery().getData().split("\\.{3}")[1]);
    }
}
